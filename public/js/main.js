/**
 * Declare globals.
 */

/**
 * Easy to type aliases for DOM query functions.
 *
 * Vanilla JavaScript can be difficult to type. The _d variable is a shim that
 * minimizes typos and makes using JS selector methods easy to use.
 */
var _d = (function (window, document) {
	// Attach selector methods to _d.
    console.log(document);
	return {
        all: document.querySelectorAll.bind(document),
        first: document.querySelector.bind(document),
        byId:  document.getElementById.bind(document),
        byClass:  document.getElementsByClassName.bind(document),
        byTag: document.getElementsByTagName.bind(document)
	};
})(window, document);



/**
 * Define core logic for application.
 */
var _app = (function (document, window) {
    // Necessary JS idiom.
    let that = this;

    // Constants hold configurable parts for the Wikipedia API URL.
    const queryDomain = 'en.wikipedia.org';
    const queryScheme = 'https';


    that.getQueryURL = function (queryTerms) {
        return `${queryScheme}://${queryDomain}/w/api.php?action=query&origin=*&list=search&srsearch=${encodeURIComponent(queryTerms)}&utf8=&format=json`;;
    };

    that.displayQuery = function (query) {
        let queryDisplays = _d.byClass('query_display');

        console.log(queryDisplays.length);
        for(let i = 0; i < queryDisplays.length; i++) {
            queryDisplays[i].innerText = query;
        }
    };
    // Return the public interface.
    return {
        /**
         * The main entry point for the application.
         */
        run: function () {
            _d.byId('query_box').oninput = function(e) {
                let query = this.value;
                let queryUrl = that.getQueryURL(query);
                fetch(queryUrl)
                .then(response => {
                    that.displayQuery(query);
                    console.log(response);
                })
                .catch(error => {console.log(error)});
            };
        },

        /**
         * .
         */

        /**
         * A sample setter.
         */
        setFoo: function (v) {
            foo = v;
        }
    };
})(document, window);



// Kick off the execution of _app. Do not modify past this comment.
(function (document, window) {
    _app.run();
})(document, window);
